## Ackee android test task
This test task that was written based on the following description: https://github.com/AckeeCZ/android-task-rick-and-morty#readme.
In this project, I used the Compose toolkit for defining the UI layer and Coroutines for handling asynchronous tasks. It is the first time I have used these libraries in a project.
Normally, I use default XML layouts and RxJava. You can find an example of my "study" e-shop application with XML/RxJava on https://gitlab.fel.cvut.cz/barysole/pda-sw-e-shop.

The application uses the modern Material Design 3 component library, and because of that, some elements look different from the design. I have decided that it is not a problem for this test task.

## APPLICATION APK
https://gitlab.fel.cvut.cz/barysole/ackee-test-task/-/blob/master/apk/app-release.apk

## KNOWN ISSUES
- Problem: The characters list doesn't accurately save its scrolling position after navigating to the character detail, add it to favorite and navigate back due to list recomposition.
