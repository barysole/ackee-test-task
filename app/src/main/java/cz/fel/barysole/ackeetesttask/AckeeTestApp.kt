package cz.fel.barysole.ackeetesttask

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class AckeeTestApp : Application()