package cz.fel.barysole.ackeetesttask

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.navigation.NavController
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import cz.fel.barysole.ackeetesttask.ui.screen.Screen
import cz.fel.barysole.ackeetesttask.ui.screen.ScreenAction
import cz.fel.barysole.ackeetesttask.ui.screen.appScreenList
import cz.fel.barysole.ackeetesttask.ui.theme.AckeeTestTaskTheme
import cz.fel.barysole.ackeetesttask.ui.uielement.main.MyNavigationBar
import cz.fel.barysole.ackeetesttask.ui.uielement.main.MyTopAppBar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.MutableSharedFlow

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        // Handle the splash screen transition. (May be helpful in the future)
        val splashScreen = installSplashScreen()
        super.onCreate(savedInstanceState)
        setContent {
            //Main screen contains the TopAppBar and other functions, which are experimental and are likely to change or to be removed in the future.
            //But in the small test app it looks nice, i guess.
            MainScreen()
        }
    }


}

// Main handler for navigation performing. Perform every (!) navigation operation in the app.
// todo: split into separate navigation methods
fun navigate(
    navController: NavController,
    currentScreen: MutableState<Screen>,
    nextScreen: Screen, args: List<Any>?,
    isSearchBarShowing: MutableState<Boolean>
) {
    if (nextScreen != currentScreen.value) {
        if (nextScreen == Screen.Characters || nextScreen == Screen.Favorite) {
            navController.navigate(nextScreen.route) {
                // Pop up to the start destination of the graph to avoid building up a large stack of destinations
                popUpTo(navController.graph.findStartDestination().id) {
                    saveState = true
                }
                launchSingleTop = true
                restoreState = true
            }
        } else if (nextScreen == Screen.CharacterDetail && !args.isNullOrEmpty()) {
            navController.navigate(Screen.CharacterDetail.routeWithoutArgument + "/" + args[0].toString()) {
                launchSingleTop = true
            }
        } else if (nextScreen == Screen.Previous) {
            navController.navigateUp()
        }
    }
    isSearchBarShowing.value = false
}

@Composable
fun MainScreen() {
    // Top bar settings block
    // Top bar will set the topBarTitle if current screen doesn't have title
    val topBarTitle = rememberSaveable {
        mutableStateOf("")
    }
    // Favorite icon state for the detail screen
    val isFavoriteIconActive = rememberSaveable {
        mutableStateOf(false)
    }
    val onTopBarTitleChange =
        remember { { newBarTitle: String -> topBarTitle.value = newBarTitle } }
    val onTopBarFavoriteIconStateChange =
        remember { { isFavoriteActive: Boolean -> isFavoriteIconActive.value = isFavoriteActive } }
    val isSearchBarShowing = rememberSaveable {
        mutableStateOf(false)
    }
    // Top bar action flow
    val actionFlow = remember { MutableSharedFlow<ScreenAction>(extraBufferCapacity = 1) }
    val onActionClick = remember {
        { action: ScreenAction ->
            actionFlow.tryEmit(action)
        }
    }

    // Navigation block.
    val navController = rememberNavController()
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    // Current screen.
    val selectedScreen = remember {
        // The initial screen.
        mutableStateOf(appScreenList.find { item -> navBackStackEntry?.destination?.hierarchy?.first()?.route == item.route }
            ?: Screen.Characters)
    }
    // Changes selectedScreen when the route changes
    navController.addOnDestinationChangedListener { _, destination, _ ->
        if (selectedScreen.value == Screen.CharacterDetail) {
            onTopBarTitleChange("")
        }
        // Take current screen from the the top of the current navigation stack after navigation
        selectedScreen.value =
            appScreenList.find { item -> destination.hierarchy.first().route == item.route }
                ?: Screen.Characters
    }
    // onScreenSelected perform all navigation in the app
    val onSelectScreen = remember {
        { screen: Screen, args: List<Any>? ->
            navigate(navController, selectedScreen, screen, args, isSearchBarShowing)
        }
    }

    // Snackbar
    val snackbarHostState = remember { SnackbarHostState() }

    // Main content
    AckeeTestTaskTheme {
        Scaffold(
            modifier = Modifier.fillMaxSize(1f),
            snackbarHost = { SnackbarHost(snackbarHostState) },
            topBar = {
                MyTopAppBar(
                    selectedScreen = selectedScreen.value,
                    onActionClick = { action -> onActionClick(action) },
                    barTitle = topBarTitle.value,
                    isFavoriteIconEnabled = isFavoriteIconActive.value,
                    isSearchBarShowing = isSearchBarShowing,
                    onSelectScreen = onSelectScreen
                )
            },
            content = { innerPadding ->
                MyNavHost(
                    navController = navController,
                    snackbarHostState = snackbarHostState,
                    modifier = Modifier.padding(innerPadding),
                    screenActionFlow = actionFlow,
                    onTopBarTitleChange = onTopBarTitleChange,
                    onTopBarFavoriteIconChange = onTopBarFavoriteIconStateChange,
                    onSelectScreen = onSelectScreen
                )
            },
            bottomBar = {
                MyNavigationBar(
                    selectedScreen = selectedScreen.value,
                    onSelectScreen = onSelectScreen
                )
            })
    }
}