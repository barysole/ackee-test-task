package cz.fel.barysole.ackeetesttask

import androidx.activity.compose.BackHandler
import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import cz.fel.barysole.ackeetesttask.ui.screen.Screen
import cz.fel.barysole.ackeetesttask.ui.screen.ScreenAction
import cz.fel.barysole.ackeetesttask.ui.screen.characterdetail.CharacterDetailScreen
import cz.fel.barysole.ackeetesttask.ui.screen.characterlist.CharacterListScreen
import cz.fel.barysole.ackeetesttask.ui.screen.favorite.FavoriteCharacterListScreen
import kotlinx.coroutines.flow.Flow

@Composable
fun MyNavHost(
    navController: NavHostController,
    snackbarHostState: SnackbarHostState,
    modifier: Modifier,
    screenActionFlow: Flow<ScreenAction>,
    onTopBarTitleChange: (String) -> Unit,
    onTopBarFavoriteIconChange: (Boolean) -> Unit,
    onSelectScreen: (Screen, List<Any>?) -> Unit
) {
    NavHost(
        navController,
        startDestination = Screen.Characters.route,
        modifier = modifier
    ) {
        composable(Screen.Characters.route) {
            CustomBackHandler(onSelectScreen)
            CharacterListScreen(
                snackbarHostState,
                onSelectItem = { characterId ->
                    onSelectScreen(Screen.CharacterDetail, listOf(characterId))
                }
            )
        }
        composable(Screen.Favorite.route) {
            CustomBackHandler(onSelectScreen)
            FavoriteCharacterListScreen(
                onSelectItem = { characterId ->
                    onSelectScreen(Screen.CharacterDetail, listOf(characterId))
                })
        }
        composable(
            Screen.CharacterDetail.route,
            arguments = listOf(navArgument("characterId") { type = NavType.LongType })
        ) { backStackEntry ->
            CustomBackHandler(onSelectScreen)
            CharacterDetailScreen(
                characterId = backStackEntry.arguments?.getLong("characterId"),
                screenActionFlow,
                onTopBarTitleChange,
                onTopBarFavoriteIconChange
            )
        }
    }
}

@Composable
fun CustomBackHandler(onSelectScreen: (Screen, List<Any>?) -> Unit) {
    BackHandler(true) {
        onSelectScreen(Screen.Previous, null)
    }
}

