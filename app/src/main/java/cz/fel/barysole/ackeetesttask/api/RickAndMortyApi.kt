package cz.fel.barysole.ackeetesttask.api

import cz.fel.barysole.ackeetesttask.api.response.CharacterListResponse
import cz.fel.barysole.ackeetesttask.model.CharacterInfo
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RickAndMortyApi {

    /**
     * Get a characters list by parameters.
     */
    @GET("character")
    suspend fun searchCharacters(
        @Query("name") name: String,
        @Query("page") page: Int,
    ): CharacterListResponse

    @GET("character")
    suspend fun getAllCharacters(
        @Query("page") page: Int,
    ): CharacterListResponse

    @GET("character/{id}")
    suspend fun getCharacterById(
        @Path("id") id: Long
    ): CharacterInfo


}