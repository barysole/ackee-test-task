package cz.fel.barysole.ackeetesttask.api.response

import com.google.gson.annotations.SerializedName
import cz.fel.barysole.ackeetesttask.model.CharacterInfo

/**
 * Data class to hold character list and pagination info retrieved from the server.
 */
data class CharacterListResponse(
    @SerializedName("info") val paginationInfo: PaginationInfo? = null,
    @SerializedName("results") val items: List<CharacterInfo> = emptyList(),
)
