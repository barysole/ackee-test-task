package cz.fel.barysole.ackeetesttask.api.response

import com.google.gson.annotations.SerializedName

/**
 * Holds server pagination info.
 */
data class PaginationInfo(
    @SerializedName("count") val count: Int = 0,
    @SerializedName("pages") val pages: Int = 0,
    //if contains null then page does not exist
    @SerializedName("next") val next: String? = null,
    //if contains null then page does not exist
    @SerializedName("prev") val prev: String? = null,
)