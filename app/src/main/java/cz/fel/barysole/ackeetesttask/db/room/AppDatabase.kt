package cz.fel.barysole.ackeetesttask.db.room

import androidx.room.Database
import androidx.room.RoomDatabase
import cz.fel.barysole.ackeetesttask.db.room.dao.CharacterDao
import cz.fel.barysole.ackeetesttask.db.room.dao.PaginationDao
import cz.fel.barysole.ackeetesttask.model.CharacterInfo
import cz.fel.barysole.ackeetesttask.model.PaginationInfo

@Database(
    entities = [CharacterInfo::class, PaginationInfo::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun characterDao(): CharacterDao
    abstract fun paginationDao(): PaginationDao
}