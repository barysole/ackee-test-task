package cz.fel.barysole.ackeetesttask.db.room.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import cz.fel.barysole.ackeetesttask.model.CharacterInfo

@Dao
interface CharacterDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(repos: CharacterInfo)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(repos: List<CharacterInfo>)

    @Query("SELECT * FROM CharacterInfo ORDER BY id ASC")
    fun getAll(): PagingSource<Int, CharacterInfo>

    @Query("SELECT * FROM CharacterInfo WHERE isFavorite = 1 ORDER BY id ASC")
    fun getFavorite(): PagingSource<Int, CharacterInfo>

    @Query("SELECT * FROM CharacterInfo WHERE id=:id")
    suspend fun getById(id: Long): CharacterInfo?

    @Query(
        "SELECT * FROM CharacterInfo WHERE " +
                "name LIKE :characterName " +
                "ORDER BY id ASC"
    )
    fun getCharactersByName(characterName: String): PagingSource<Int, CharacterInfo>

    @Query("DELETE FROM CharacterInfo WHERE isFavorite = 0")
    suspend fun deleteNotFavorite()
}