package cz.fel.barysole.ackeetesttask.db.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import cz.fel.barysole.ackeetesttask.model.PaginationInfo

@Dao
interface PaginationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(paginationInfo: PaginationInfo)

    @Query(
        "SELECT * FROM PaginationInfo WHERE " +
                "paginationName LIKE :paginationName"
    )
    suspend fun getPaginationInfoFor(paginationName: String): PaginationInfo?

}