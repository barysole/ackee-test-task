package cz.fel.barysole.ackeetesttask.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "CharacterInfo")
data class CharacterInfo(
    @PrimaryKey @field:SerializedName("id") val id: Long,
    @field:SerializedName("name") val name: String?,
    @field:SerializedName("status") val status: String?,
    @field:SerializedName("image") val image: String?,
    @field:SerializedName("species") val species: String?,
    @field:SerializedName("type") val type: String?,
    @field:SerializedName("gender") val gender: String?,
    @field:SerializedName("origin") @Embedded val origin: Origin?,
    @field:SerializedName("location") @Embedded val location: Location?,
    // Locally saved favorite state
    var isFavorite: Boolean = false,
)
