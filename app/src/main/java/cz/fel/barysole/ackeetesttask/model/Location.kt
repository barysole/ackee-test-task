package cz.fel.barysole.ackeetesttask.model

import com.google.gson.annotations.SerializedName

data class Location(
    @field:SerializedName("name") val locationName: String?
)
