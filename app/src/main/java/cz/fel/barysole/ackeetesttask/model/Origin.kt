package cz.fel.barysole.ackeetesttask.model

import com.google.gson.annotations.SerializedName

data class Origin(
    @field:SerializedName("name") val originName: String?
)