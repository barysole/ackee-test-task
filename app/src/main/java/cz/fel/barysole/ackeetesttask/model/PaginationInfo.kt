package cz.fel.barysole.ackeetesttask.model

import androidx.room.Entity
import androidx.room.PrimaryKey

// Info to track pagination
@Entity(tableName = "PaginationInfo")
data class PaginationInfo(
    @PrimaryKey val paginationName: String,
    val previousNotDownloadedPage: Int?,
    val nextNotDownloadedPage: Int?
)
