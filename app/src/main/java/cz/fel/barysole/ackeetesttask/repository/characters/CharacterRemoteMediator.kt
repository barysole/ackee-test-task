package cz.fel.barysole.ackeetesttask.repository.characters

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import cz.fel.barysole.ackeetesttask.api.RickAndMortyApi
import cz.fel.barysole.ackeetesttask.db.room.AppDatabase
import cz.fel.barysole.ackeetesttask.model.CharacterInfo
import cz.fel.barysole.ackeetesttask.model.PaginationInfo
import cz.fel.barysole.ackeetesttask.repository.characters.CharacterRepositoryImpl.Companion.insertCharacterItems
import cz.fel.barysole.ackeetesttask.repository.pagination.PaginationDataType
import retrofit2.HttpException
import java.io.IOException


@OptIn(ExperimentalPagingApi::class)
class CharacterRemoteMediator(
    private val rickAndMortyApi: RickAndMortyApi,
    private val appDatabase: AppDatabase
) : RemoteMediator<Int, CharacterInfo>() {

    override suspend fun initialize(): InitializeAction {
        // The initial refresh setting
        return InitializeAction.LAUNCH_INITIAL_REFRESH
    }

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, CharacterInfo>
    ): MediatorResult {
        // retrieve cached pagination info
        val currentPaginationInfo =
            appDatabase.paginationDao()
                .getPaginationInfoFor(PaginationDataType.SearchCharacterPagination.paginationName)
        // compute required page index
        val requiredPage = when (loadType) {
            LoadType.REFRESH -> {
                state.anchorPosition ?: STARTING_PAGE_NUMBER
            }
            // REFRESH will always load the first page in the list, so prepend is no longer needed.
            LoadType.PREPEND -> {
                return MediatorResult.Success(endOfPaginationReached = true)
            }

            LoadType.APPEND -> {
                val lastPage = state.pages.lastOrNull()
                val nextKey = lastPage?.nextKey
                // if next key is null, then there is no more items in DB
                if (nextKey == null) {
                    // if we can get page from network
                    if (currentPaginationInfo?.nextNotDownloadedPage == null) {
                        return MediatorResult.Success(endOfPaginationReached = true)
                    } else {
                        currentPaginationInfo.nextNotDownloadedPage
                    }
                } else {
                    nextKey
                }
            }
        }

        try {
            val response = rickAndMortyApi.getAllCharacters(requiredPage)
            val endOfPaginationReached = response.paginationInfo?.next == null
            appDatabase.withTransaction {
                if (loadType == LoadType.REFRESH) {
                    appDatabase.characterDao().deleteNotFavorite()
                }
                appDatabase.paginationDao().insert(
                    PaginationInfo(
                        PaginationDataType.SearchCharacterPagination.paginationName,
                        // we always start at first page
                        null,
                        if (response.paginationInfo?.next == null) null else requiredPage + 1
                    )
                )
                insertCharacterItems(response.items, appDatabase.characterDao())
            }
            return MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
        } catch (exception: IOException) {
            return MediatorResult.Error(exception)
        } catch (exception: HttpException) {
            return MediatorResult.Error(exception)
        }
    }

    companion object {
        const val STARTING_PAGE_NUMBER = 1
    }
}