package cz.fel.barysole.ackeetesttask.repository.characters

import androidx.paging.PagingData
import cz.fel.barysole.ackeetesttask.model.CharacterInfo
import kotlinx.coroutines.flow.Flow

interface CharacterRepository {

    fun getCharactersByNameFlow(characterNameQuery: String): Flow<PagingData<CharacterInfo>>

    fun getCharactersFlow(): Flow<PagingData<CharacterInfo>>

    fun getFavoriteCharactersFlow(): Flow<PagingData<CharacterInfo>>

    suspend fun getCharacter(characterId: Long): CharacterInfo?

    suspend fun changeCharacterFavoriteStatus(characterId: Long)

}