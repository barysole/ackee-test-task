package cz.fel.barysole.ackeetesttask.repository.characters

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.room.withTransaction
import cz.fel.barysole.ackeetesttask.api.RickAndMortyApi
import cz.fel.barysole.ackeetesttask.db.room.AppDatabase
import cz.fel.barysole.ackeetesttask.db.room.dao.CharacterDao
import cz.fel.barysole.ackeetesttask.model.CharacterInfo
import kotlinx.coroutines.flow.Flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CharacterRepositoryImpl @Inject constructor(
    private val rickAndMortyApi: RickAndMortyApi,
    private val appDatabase: AppDatabase
) : CharacterRepository {

    // Find characters by name query. Works without DB.
    override fun getCharactersByNameFlow(characterNameQuery: String): Flow<PagingData<CharacterInfo>> {
        return Pager(
            config = PagingConfig(
                pageSize = NETWORK_PAGE_SIZE,
                prefetchDistance = NETWORK_PAGE_SIZE,
                initialLoadSize = NETWORK_PAGE_SIZE
            ),
            pagingSourceFactory = {
                CharactersSearchPagingSource(
                    rickAndMortyApi,
                    characterNameQuery
                )
            }
        ).flow
    }

    override fun getCharactersFlow(): Flow<PagingData<CharacterInfo>> {
        @OptIn(ExperimentalPagingApi::class)
        return Pager(
            config = PagingConfig(pageSize = NETWORK_PAGE_SIZE,
                prefetchDistance = NETWORK_PAGE_SIZE,
                initialLoadSize = NETWORK_PAGE_SIZE),
            remoteMediator = CharacterRemoteMediator(
                rickAndMortyApi,
                appDatabase
            ),
            pagingSourceFactory = { appDatabase.characterDao().getAll() }
        ).flow
    }

    override fun getFavoriteCharactersFlow(): Flow<PagingData<CharacterInfo>> {
        return Pager(
            config = PagingConfig(
                pageSize = NETWORK_PAGE_SIZE,
                prefetchDistance = NETWORK_PAGE_SIZE,
                initialLoadSize = NETWORK_PAGE_SIZE
            ),
            pagingSourceFactory = { appDatabase.characterDao().getFavorite() }
        ).flow
    }

    override suspend fun getCharacter(characterId: Long): CharacterInfo? {
        try {
            val response = rickAndMortyApi.getCharacterById(characterId)
            appDatabase.withTransaction {
                insertCharacterItems(listOf(response), appDatabase.characterDao())
            }
        } catch (exception: IOException) {
            //todo: add exception handling
        } catch (exception: HttpException) {
            //todo: add exception handling
        }
        return appDatabase.characterDao().getById(characterId)
    }

    override suspend fun changeCharacterFavoriteStatus(characterId: Long) {
        appDatabase.withTransaction {
            val itemInDb = appDatabase.characterDao().getById(characterId)
            itemInDb?.let {
                appDatabase.characterDao().insert(it.copy(isFavorite = !itemInDb.isFavorite))
            }
        }
    }

    companion object {
        //based on this description - https://rickandmortyapi.com/documentation/#info-and-pagination
        const val NETWORK_PAGE_SIZE = 20

        // Save isFavorite flag during items update in the DB
        suspend fun insertCharacterItems(items: List<CharacterInfo>, characterDao: CharacterDao) {
            // clear the character table in the database
            for (item in items) {
                val itemInDb = characterDao.getById(item.id)
                if (itemInDb?.isFavorite == true) {
                    characterDao.insert(item.copy(isFavorite = true))
                } else {
                    characterDao.insert(item)
                }
            }
        }
    }

}