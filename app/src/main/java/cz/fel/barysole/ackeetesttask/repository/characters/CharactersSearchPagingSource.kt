package cz.fel.barysole.ackeetesttask.repository.characters

import androidx.paging.PagingSource
import androidx.paging.PagingState
import cz.fel.barysole.ackeetesttask.api.RickAndMortyApi
import cz.fel.barysole.ackeetesttask.model.CharacterInfo
import retrofit2.HttpException
import java.io.IOException

class CharactersSearchPagingSource(
    private val rickAndMortyApi: RickAndMortyApi,
    private val characterName: String
) : PagingSource<Int, CharacterInfo>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, CharacterInfo> {
        val pageNumber = params.key ?: STARTING_PAGE_NUMBER
        return try {
            val response = rickAndMortyApi.searchCharacters(characterName, pageNumber)
            val nextKey = if (response.paginationInfo?.next == null) null else pageNumber + 1
            LoadResult.Page(
                data = response.items,
                prevKey = if (response.paginationInfo?.prev == null) null else pageNumber - 1,
                nextKey = nextKey
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            if (exception.code() == 404) {
                return LoadResult.Page(
                    data = emptyList(),
                    prevKey = null,
                    nextKey = null
                )
            }
            return LoadResult.Error(exception)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, CharacterInfo>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

    companion object {
        const val STARTING_PAGE_NUMBER = 1;
    }

}