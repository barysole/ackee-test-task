package cz.fel.barysole.ackeetesttask.repository.pagination

enum class PaginationDataType(val paginationName: String) {
    SearchCharacterPagination("SearchCharacterPagination"),
}