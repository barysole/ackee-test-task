package cz.fel.barysole.ackeetesttask.ui.screen

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import cz.fel.barysole.ackeetesttask.R

sealed class Screen(
    val route: String = "",
    @StringRes val nameResId: Int? = null,
    @DrawableRes val iconResId: Int? = null,
    val routeWithoutArgument: String? = null,
    val isBackButtonShowing: Boolean = false,
    val actionList: List<ScreenAction> = emptyList(),
    val showTopAppBar: Boolean = true,
    val showBottomBar: Boolean = true
) {

    object Previous : Screen()

    object Characters : Screen(
        "characters",
        R.string.characters,
        R.drawable.ic_rick_face,
        actionList = listOf(ScreenAction.SearchCharacterScreenAction)
    )

    object Favorite : Screen(
        "favorite",
        R.string.favorite,
        R.drawable.baseline_star_24
    )

    object CharacterDetail : Screen(
        "characterdetail/{characterId}",
        routeWithoutArgument = "characterdetail",
        actionList = listOf(ScreenAction.AddToFavoriteScreenAction),
        showBottomBar = false,
        isBackButtonShowing = true
    )
}

enum class ScreenAction {
    SearchCharacterScreenAction,
    AddToFavoriteScreenAction
}

val appScreenList = listOf(
    Screen.Characters,
    Screen.Favorite,
    Screen.CharacterDetail
)