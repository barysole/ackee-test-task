package cz.fel.barysole.ackeetesttask.ui.screen.characterdetail

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import cz.fel.barysole.ackeetesttask.ui.screen.ScreenAction
import kotlinx.coroutines.flow.Flow

@Composable
fun CharacterDetailScreen(
    characterId: Long?,
    screenActionFlow: Flow<ScreenAction>,
    onTopBarTitleChange: (String) -> Unit,
    onTopBarFavoriteIconChange: (Boolean) -> Unit,
    viewModel: CharacterDetailViewModel = hiltViewModel()
) {
    viewModel.initialize(
        characterId,
        onTopBarTitleChange,
        onTopBarFavoriteIconChange,
        screenActionFlow
    )
    val characterDetailScreenState by viewModel.uiState.collectAsState()
    Surface(
        shape = MaterialTheme.shapes.large,
        color = MaterialTheme.colorScheme.surface,
        shadowElevation = 4.dp,
        modifier = Modifier
            .padding(horizontal = 8.dp, vertical = 8.dp)
            .fillMaxSize(1f)
    ) {
        characterDetailScreenState.characterInfo?.let { character ->
            CharacterInfo(character = character, 16.dp, 16.dp, 24.dp)
        }
    }
}