package cz.fel.barysole.ackeetesttask.ui.screen.characterdetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.fel.barysole.ackeetesttask.model.CharacterInfo
import cz.fel.barysole.ackeetesttask.repository.characters.CharacterRepository
import cz.fel.barysole.ackeetesttask.ui.screen.ScreenAction
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class CharacterDetailViewModel @Inject constructor(
    private val characterRepository: CharacterRepository
) : ViewModel() {

    private var isInit: Boolean = false

    // The UI state
    private val _uiState = MutableStateFlow(UiState())
    val uiState: StateFlow<UiState> = _uiState.asStateFlow()
    var onTopBarTitleChange: (String) -> Unit = {}
    var onTopBarFavoriteIconChange: (Boolean) -> Unit = {}

    fun initialize(
        characterId: Long?,
        onTopBarTitleChange: (String) -> Unit = {},
        onTopBarFavoriteIconChange: (Boolean) -> Unit = {},
        screenFlow: Flow<ScreenAction>? = null
    ) {
        if (!isInit) {
            this.onTopBarTitleChange = onTopBarTitleChange
            this.onTopBarFavoriteIconChange = onTopBarFavoriteIconChange
            if (screenFlow != null) {
                listenScreenFlow(screenFlow)
            }
            onRefreshData(characterId)
            isInit = true
        }
    }

    fun showLoadingError(hasError: Boolean) {
        _uiState.update { state -> state.copy(showError = hasError) }
    }

    fun onRefreshData(characterId: Long?) {
        showLoadingError(false)
        characterId?.let {
            viewModelScope.launch {
                val characterInfo = withContext(Dispatchers.IO) {
                    characterRepository.getCharacter(
                        characterId
                    )
                }
                onTopBarTitleChange(characterInfo?.name ?: "")
                onTopBarFavoriteIconChange(characterInfo?.isFavorite ?: false)
                _uiState.update { state ->
                    state.copy(
                        characterInfo = characterInfo
                    )
                }
            }
        }
    }

    fun listenScreenFlow(screenActionFlow: Flow<ScreenAction>) {
        viewModelScope.launch {
            screenActionFlow.collect { action ->
                if (action == ScreenAction.AddToFavoriteScreenAction) {
                    changeCharacterFavoriteStatus()
                }
            }
        }
    }

    fun changeCharacterFavoriteStatus() {
        viewModelScope.launch {
            uiState.value.characterInfo?.let {
                characterRepository.changeCharacterFavoriteStatus(it.id)
                onRefreshData(it.id)
            }
        }
    }

    data class UiState(
        val showError: Boolean = false,
        val characterInfo: CharacterInfo? = null
    )

}