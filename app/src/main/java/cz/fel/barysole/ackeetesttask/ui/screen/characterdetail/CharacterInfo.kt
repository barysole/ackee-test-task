package cz.fel.barysole.ackeetesttask.ui.screen.characterdetail

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import cz.fel.barysole.ackeetesttask.R
import cz.fel.barysole.ackeetesttask.model.CharacterInfo
import cz.fel.barysole.ackeetesttask.model.Location
import cz.fel.barysole.ackeetesttask.model.Origin

@Composable
@OptIn(ExperimentalGlideComposeApi::class)
fun CharacterInfo(
    character: CharacterInfo,
    startPaddingForElements: Dp = 0.dp,
    topPaddingForElements: Dp = 0.dp,
    itemSpaceSize: Dp = 0.dp
) {
    Column(
        modifier = Modifier
            .fillMaxWidth(1f)
            .verticalScroll(rememberScrollState())
    ) {
        Row {
            character.image?.let {
                GlideImage(
                    model = it,
                    contentDescription = stringResource(R.string.character_image_description),
                    modifier = Modifier
                        .padding(top = topPaddingForElements, start = startPaddingForElements)
                        .size(128.dp)
                        .clip(MaterialTheme.shapes.small)
                )
            }
            Column(
                modifier = Modifier.padding(
                    top = topPaddingForElements,
                    start = startPaddingForElements
                )
            ) {
                Text(
                    text = stringResource(R.string.name_field_title),
                    style = MaterialTheme.typography.bodyLarge,
                    color = MaterialTheme.colorScheme.secondary,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis

                )
                Text(
                    modifier = Modifier.padding(top = 8.dp),
                    text = "${character.name}",
                    style = MaterialTheme.typography.titleLarge,
                    fontWeight = FontWeight.Bold,
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis

                )
                Spacer(modifier = Modifier.height(4.dp))
            }
        }
        Divider(modifier = Modifier.padding(top = 16.dp, bottom = 16.dp))
        Row(modifier = Modifier.padding(start = startPaddingForElements)) {
            Column {
                Text(
                    text = stringResource(R.string.status_field_title),
                    style = MaterialTheme.typography.bodyMedium,
                    color = MaterialTheme.colorScheme.secondary,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis

                )
                Text(
                    text = "${character.status}",
                    style = MaterialTheme.typography.titleMedium,
                    fontWeight = FontWeight.Bold,
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis

                )
                Spacer(modifier = Modifier.height(itemSpaceSize))
                Text(
                    text = stringResource(R.string.type_field_title),
                    style = MaterialTheme.typography.bodyMedium,
                    color = MaterialTheme.colorScheme.secondary,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis

                )
                Text(
                    text = if (character.type.isNullOrBlank()) "-" else character.type,
                    style = MaterialTheme.typography.titleMedium,
                    fontWeight = FontWeight.Bold,
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis

                )
                Spacer(modifier = Modifier.height(itemSpaceSize))
                Text(
                    text = stringResource(R.string.gender_field_title),
                    style = MaterialTheme.typography.bodyMedium,
                    color = MaterialTheme.colorScheme.secondary,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis

                )
                Text(
                    text = "${character.gender}",
                    style = MaterialTheme.typography.titleMedium,
                    fontWeight = FontWeight.Bold,
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis

                )
                Spacer(modifier = Modifier.height(itemSpaceSize))
                Text(
                    text = stringResource(R.string.origin_field_title),
                    style = MaterialTheme.typography.bodyMedium,
                    color = MaterialTheme.colorScheme.secondary,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis

                )
                Text(
                    text = "${character.origin?.originName}",
                    style = MaterialTheme.typography.titleMedium,
                    fontWeight = FontWeight.Bold,
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis

                )
                Spacer(modifier = Modifier.height(itemSpaceSize))
                Text(
                    text = stringResource(R.string.location_field_title),
                    style = MaterialTheme.typography.bodyMedium,
                    color = MaterialTheme.colorScheme.secondary,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis

                )
                Text(
                    text = "${character.location?.locationName}",
                    style = MaterialTheme.typography.titleMedium,
                    fontWeight = FontWeight.Bold,
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis
                )
                Spacer(modifier = Modifier.height(itemSpaceSize))
            }
        }
    }
}

@Preview
@Composable
fun CharacterInfoPreview() {
    CharacterInfo(
        character = CharacterInfo(
            id = 2,
            name = "Morty Smith",
            status = "Alive",
            species = "Human",
            type = "",
            gender = "Male",
            origin = Origin("Earth"),
            location = Location("Earth"),
            image = null
        )
    )
}