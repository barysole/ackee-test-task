package cz.fel.barysole.ackeetesttask.ui.screen.characterlist

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.paging.LoadState
import androidx.paging.compose.collectAsLazyPagingItems
import cz.fel.barysole.ackeetesttask.R
import cz.fel.barysole.ackeetesttask.ui.uielement.character.CharacterList

@Composable
fun CharacterListScreen(
    snackbarHostState: SnackbarHostState,
    onSelectItem: (id: Long) -> Unit,
    characterListViewModel: CharacterListViewModel = hiltViewModel()
) {
    Surface(
        color = MaterialTheme.colorScheme.background
    ) {
        val characterItems = characterListViewModel.charactersPDFlow.collectAsLazyPagingItems()
        if (characterItems.loadState.source.append is LoadState.Error) {
            characterListViewModel.showLoadingError(true)
        } else {
            characterListViewModel.showLoadingError(false)
        }
        // If the UI state contains an error, show snackbar
        val characterListScreenState by characterListViewModel.uiState.collectAsState()
        if (characterListScreenState.showError) {
            val errorMessage = stringResource(R.string.data_cannot_be_loaded_error)
            LaunchedEffect(snackbarHostState) {
                snackbarHostState.showSnackbar(
                    message = errorMessage
                )
            }
        }
        // todo: there is no pullToRefresh in MD3 at this moment. Add it later. :c
        if (characterItems.itemCount == 0 && characterItems.loadState.refresh != LoadState.Loading) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Button(
                    onClick = { characterListViewModel.refreshList() },
                    colors = ButtonDefaults.buttonColors(
                        containerColor = MaterialTheme.colorScheme.surface,
                        contentColor = MaterialTheme.colorScheme.primary
                    )
                ) {
                    Text(stringResource(R.string.reload_text))
                }
            }
        } else {
            CharacterList(characterItems, onSelectItem)
        }
    }
}