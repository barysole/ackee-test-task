package cz.fel.barysole.ackeetesttask.ui.screen.characterlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import cz.fel.barysole.ackeetesttask.model.CharacterInfo
import cz.fel.barysole.ackeetesttask.repository.characters.CharacterRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@OptIn(ExperimentalCoroutinesApi::class)
@HiltViewModel
class CharacterListViewModel @Inject constructor(
    private val characterRepository: CharacterRepository
) : ViewModel() {

    // The UI state
    private val _uiState = MutableStateFlow(UiState())
    val uiState: StateFlow<UiState> = _uiState.asStateFlow()
    val charactersPDFlow: Flow<PagingData<CharacterInfo>>

    private val getAllCharacters = MutableSharedFlow<Unit>()

    init {
        charactersPDFlow = getAllCharacters
            .onStart { emit(Unit) }
            .flatMapLatest {
                withContext(Dispatchers.IO) {
                    characterRepository.getCharactersFlow()
                }
            }
            .cachedIn(viewModelScope)
    }

    fun showLoadingError(hasError: Boolean) {
        _uiState.value = UiState(showError = hasError)
    }

    fun refreshList() {
        viewModelScope.launch {
            showLoadingError(false)
            getAllCharacters.emit(Unit)
        }
    }

    data class UiState(
        val showError: Boolean = false
    )

}