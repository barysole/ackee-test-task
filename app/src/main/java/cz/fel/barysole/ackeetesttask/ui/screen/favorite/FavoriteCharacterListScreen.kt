package cz.fel.barysole.ackeetesttask.ui.screen.favorite

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.paging.compose.collectAsLazyPagingItems
import cz.fel.barysole.ackeetesttask.R
import cz.fel.barysole.ackeetesttask.ui.uielement.character.CharacterList

@Composable
fun FavoriteCharacterListScreen(
    onSelectItem: (id: Long) -> Unit,
    favoriteCharacterListViewModel: FavoriteCharacterListViewModel = hiltViewModel()
) {
    Surface(
        color = MaterialTheme.colorScheme.background
    ) {
        val favoriteCharacters =
            favoriteCharacterListViewModel.favoriteCharactersPDFlow.collectAsLazyPagingItems()
        if (favoriteCharacters.itemCount == 0) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(stringResource(R.string.no_favorite_characters_text))
            }
        } else {
            CharacterList(favoriteCharacters, onSelectItem)
        }
    }
}