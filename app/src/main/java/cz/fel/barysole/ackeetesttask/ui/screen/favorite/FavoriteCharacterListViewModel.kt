package cz.fel.barysole.ackeetesttask.ui.screen.favorite

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import cz.fel.barysole.ackeetesttask.model.CharacterInfo
import cz.fel.barysole.ackeetesttask.repository.characters.CharacterRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@OptIn(ExperimentalCoroutinesApi::class)
@HiltViewModel
class FavoriteCharacterListViewModel @Inject constructor(
    private val characterRepository: CharacterRepository
) : ViewModel() {

    val favoriteCharactersPDFlow: Flow<PagingData<CharacterInfo>>

    private val getFavoriteCharacters = MutableSharedFlow<Unit>()

    init {
        favoriteCharactersPDFlow = getFavoriteCharacters
            .onStart { emit(Unit) }
            .flatMapLatest {
                withContext(Dispatchers.IO) {
                    characterRepository.getFavoriteCharactersFlow()
                }
            }
            .cachedIn(viewModelScope)
    }

    fun refreshList() {
        viewModelScope.launch { getFavoriteCharacters.emit(Unit) }
    }

}