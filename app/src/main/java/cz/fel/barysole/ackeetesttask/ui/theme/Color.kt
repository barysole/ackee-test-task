package cz.fel.barysole.ackeetesttask.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Pink40 = Color(0xFF7D5260)

val White = Color(0xFFFFFFFF)
val DarkGray = Color(0xFF212224)
val LightGray = Color(0xFFF4F4F9)
val Gray = Color(0xFFbCBCBC)

val Blue = Color(0xFF1818FF)