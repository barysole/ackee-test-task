package cz.fel.barysole.ackeetesttask.ui.uielement.character

import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.paging.compose.LazyPagingItems
import cz.fel.barysole.ackeetesttask.model.CharacterInfo

@Composable
fun CharacterList(
    characterList: LazyPagingItems<CharacterInfo>,
    onSelectItem: (id: Long) -> Unit
) {
    LazyColumn() {
        items(
            count = characterList.itemCount,
            contentType = { if (characterList[it] == null) 1 else 0 }
        ) { index ->
            characterList[index]?.let {
                CharacterListItem(it, onSelectItem)
            }
        }
    }
}