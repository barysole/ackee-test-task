package cz.fel.barysole.ackeetesttask.ui.uielement.charactersearch

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import cz.fel.barysole.ackeetesttask.model.CharacterInfo
import cz.fel.barysole.ackeetesttask.repository.characters.CharacterRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filterNot
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@OptIn(ExperimentalCoroutinesApi::class)
@HiltViewModel
class CharacterSearchViewModel @Inject
constructor(
    private val characterRepository: CharacterRepository
) : ViewModel() {

    // The UI state
    private val _uiState = MutableStateFlow(UiState())
    val uiState: StateFlow<UiState> = _uiState.asStateFlow()
    val pagingDataFlow: Flow<PagingData<CharacterInfo>>

    private val searchCharacterByName = MutableSharedFlow<String>()

    init {
        pagingDataFlow = searchCharacterByName
            .distinctUntilChanged()
            .onEach { characterName ->
                _uiState.value = UiState(query = characterName)
            }
            .filterNot { characterName -> characterName.isBlank() }
            .flatMapLatest { characterName ->
                withContext(Dispatchers.IO) {
                    characterRepository.getCharactersByNameFlow(characterName)
                }
            }
            .cachedIn(viewModelScope)
    }

    fun onCharacterSearch(characterName: String) {
        viewModelScope.launch { searchCharacterByName.emit(characterName) }
    }

    fun onFieldClean() {
        _uiState.value = UiState(query = "")
    }

}

data class UiState(
    val query: String = ""
)