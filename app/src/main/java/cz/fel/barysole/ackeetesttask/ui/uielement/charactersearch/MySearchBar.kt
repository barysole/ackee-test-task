package cz.fel.barysole.ackeetesttask.ui.uielement.charactersearch

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.SearchBar
import androidx.compose.material3.SearchBarDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.paging.LoadState
import androidx.paging.compose.collectAsLazyPagingItems
import cz.fel.barysole.ackeetesttask.R
import cz.fel.barysole.ackeetesttask.ui.screen.Screen
import cz.fel.barysole.ackeetesttask.ui.uielement.character.CharacterList

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MySearchBar(
    isSearchBarShowing: MutableState<Boolean>,
    onSelectScreen: (Screen, List<Any>?) -> Unit,
    characterSearchViewModel: CharacterSearchViewModel = hiltViewModel()
) {
    val isSearchBarIsActive = rememberSaveable {
        mutableStateOf(false)
    }
    val searchBarState by characterSearchViewModel.uiState.collectAsState()
    // clean the search field after bar closing
    if (!isSearchBarShowing.value) {
        characterSearchViewModel.onFieldClean()
    }
    SearchBar(
        modifier = Modifier
            .fillMaxWidth(1f)
            .padding(if (isSearchBarIsActive.value) 0.dp else 8.dp),
        query = searchBarState.query,
        onQueryChange = { characterSearchViewModel.onCharacterSearch(it) },
        onSearch = { characterSearchViewModel.onCharacterSearch(it) },
        active = isSearchBarIsActive.value,
        onActiveChange = { isSearchBarIsActive.value = it },
        placeholder = {
            Text(text = stringResource(R.string.search_bar_placeholder))
        },
        leadingIcon = {
            Icon(
                painterResource(R.drawable.baseline_arrow_back_ios_new_24),
                modifier = Modifier.clickable { isSearchBarShowing.value = false },
                contentDescription = stringResource(R.string.arrow_back_icon_description)
            )
        },
        trailingIcon = {
            Icon(
                painterResource(R.drawable.baseline_close_24),
                contentDescription = stringResource(R.string.close_icon_description),
                modifier = Modifier.clickable {
                    if (searchBarState.query.isNotBlank()) {
                        characterSearchViewModel.onFieldClean()
                    } else {
                        isSearchBarShowing.value = false
                    }
                }
            )
        },
        tonalElevation = 0.dp,
        colors = if (isSearchBarIsActive.value) SearchBarDefaults.colors(containerColor = MaterialTheme.colorScheme.background) else SearchBarDefaults.colors()
    ) {
        val characterListItems = characterSearchViewModel.pagingDataFlow.collectAsLazyPagingItems()
        // do not show elements when the query is empty
        if (searchBarState.query.isNotBlank()) {
            if (characterListItems.loadState.refresh is LoadState.Error) {
                Box(
                    modifier = Modifier
                        .fillMaxWidth(1f)
                        .padding(vertical = 64.dp),
                    contentAlignment = Alignment.Center
                ) {
                    Text(
                        stringResource(R.string.data_cannot_be_loaded_error),
                        style = MaterialTheme.typography.titleMedium,
                        textAlign = TextAlign.Center
                    )
                }
            } else {
                CharacterList(characterListItems) { characterId ->
                    onSelectScreen(
                        Screen.CharacterDetail,
                        listOf(characterId)
                    )
                }
            }
        }
    }
}
