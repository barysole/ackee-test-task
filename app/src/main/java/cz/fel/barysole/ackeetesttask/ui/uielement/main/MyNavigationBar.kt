package cz.fel.barysole.ackeetesttask.ui.uielement.main

import androidx.compose.foundation.layout.size
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.NavigationBarItemDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import cz.fel.barysole.ackeetesttask.R
import cz.fel.barysole.ackeetesttask.ui.screen.Screen


@Composable
fun MyNavigationBar(
    selectedScreen: Screen,
    onSelectScreen: (Screen, List<Any>?) -> Unit
) {
    val navBarItems = remember {
        listOf(
            Screen.Characters,
            Screen.Favorite
        )
    }
    if (selectedScreen.showBottomBar) {
        NavigationBar(
            modifier = Modifier.shadow(8.dp),
            containerColor = MaterialTheme.colorScheme.surface,
            tonalElevation = 0.dp
        ) {
            navBarItems.forEach { screen ->
                NavigationBarItem(
                    icon = {
                        if (screen.iconResId != null) {
                            Icon(
                                painterResource(screen.iconResId),
                                contentDescription = stringResource(
                                    screen.nameResId ?: R.string.empty_string
                                ),
                                modifier = Modifier.size(24.dp)
                            )
                        }
                    },
                    colors = NavigationBarItemDefaults.colors(selectedIconColor = Color.Blue),
                    label = { Text(stringResource(screen.nameResId ?: R.string.empty_string)) },
                    selected = selectedScreen == screen,
                    onClick = {
                        onSelectScreen(screen, null)
                    }
                )
            }
        }
    }
}

@Preview
@Composable
fun MyNavigationBarPreview() {
    MyNavigationBar(
        Screen.Characters
    ) { _, _ -> }
}