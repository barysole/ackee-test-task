package cz.fel.barysole.ackeetesttask.ui.uielement.main

import android.annotation.SuppressLint
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import cz.fel.barysole.ackeetesttask.R
import cz.fel.barysole.ackeetesttask.ui.screen.Screen
import cz.fel.barysole.ackeetesttask.ui.screen.ScreenAction
import cz.fel.barysole.ackeetesttask.ui.uielement.charactersearch.MySearchBar

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MyTopAppBar(
    selectedScreen: Screen,
    onActionClick: (ScreenAction) -> Unit,
    isFavoriteIconEnabled: Boolean = false,
    barTitle: String = "",
    // Can be modified internally
    isSearchBarShowing: MutableState<Boolean>,
    onSelectScreen: (Screen, List<Any>?) -> Unit
) {
    if (selectedScreen.showTopAppBar) {
        Crossfade(targetState = isSearchBarShowing.value) { showSearchBar ->
            when (showSearchBar) {
                // showing iff isSearchBarShowing is true
                true -> MySearchBar(isSearchBarShowing, onSelectScreen)
                false -> TopAppBar(
                    modifier = Modifier.shadow(16.dp),
                    navigationIcon = {
                        if (selectedScreen.isBackButtonShowing) {
                            Icon(
                                painterResource(R.drawable.baseline_arrow_back_ios_new_24),
                                modifier = Modifier
                                    .clickable { onSelectScreen(Screen.Previous, null) }
                                    .padding(horizontal = 8.dp),
                                contentDescription = stringResource(R.string.arrow_back_icon_description)
                            )
                        }
                    },
                    title = {
                        Text(
                            //there is no null in the screenWithTopAppBarList
                            text = if (selectedScreen.nameResId != null) stringResource(
                                selectedScreen.nameResId
                            ) else barTitle,
                            maxLines = 1,
                            overflow = TextOverflow.Ellipsis
                        )
                    },
                    actions = {
                        TopBarActionButtons(
                            actionButtonList = selectedScreen.actionList,
                            onActionClick,
                            isFavoriteIconEnabled,
                            onSearchBarClick = { isSearchBarShowing.value = true }
                        )
                    },
                    colors = TopAppBarDefaults.topAppBarColors(containerColor = MaterialTheme.colorScheme.background)
                )
            }
        }
    }
}

@SuppressLint("UnrememberedMutableState")
@Preview
@Composable
fun MyTopAppBarPreview() {
    MyTopAppBar(
        Screen.Characters,
        {},
        isSearchBarShowing = mutableStateOf(false),
        onSelectScreen = { _, _ -> })
}