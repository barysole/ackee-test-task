package cz.fel.barysole.ackeetesttask.ui.uielement.main

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import cz.fel.barysole.ackeetesttask.R
import cz.fel.barysole.ackeetesttask.ui.screen.ScreenAction


@Composable
fun TopBarActionButtons(
    actionButtonList: List<ScreenAction>,
    onActionClick: (ScreenAction) -> Unit,
    isFavoriteIconEnabled: Boolean = false,
    onSearchBarClick: () -> Unit = {}
) {
    for (screenAction in actionButtonList) {
        if (screenAction == ScreenAction.SearchCharacterScreenAction) {
            IconButton(
                onClick = {
                    onActionClick(ScreenAction.SearchCharacterScreenAction)
                    onSearchBarClick()
                }) {
                Icon(
                    imageVector = Icons.Default.Search,
                    contentDescription = stringResource(R.string.search_button_description)
                )
            }
        } else if (screenAction == ScreenAction.AddToFavoriteScreenAction) {
            IconButton(onClick = { onActionClick(ScreenAction.AddToFavoriteScreenAction) }) {
                Icon(
                    painter = if (isFavoriteIconEnabled) painterResource(R.drawable.baseline_star_24) else painterResource(
                        R.drawable.baseline_star_outline_24
                    ),
                    contentDescription = stringResource(R.string.favorite_button_description),
                    tint = Color.Blue
                )
            }
        }
    }
}